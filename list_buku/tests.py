from django.test import TestCase
from django.urls import resolve
from . import views

class ListBukuTest(TestCase):
    def test_url_exist(self):
        response = self.client.get('/rekomendasi/')
        print()
        self.assertEqual(response.status_code, 200)

    def test_correct_views_function(self):
        found = resolve('/list_buku/')
        self.assertEqual(found.func, views.list_buku)

    def test_using_right_template(self):
        response = self.client.get('/list_buku/')
        self.assertTemplateUsed(response, 'list_buku.html')

    




