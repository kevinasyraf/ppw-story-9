from django.shortcuts import render

def index(request):
	context = {
		'judul' : 'Home',
		'subjudul' : 'Welcome! Please log in if you have already signed up'
	}
	return render(request, 'home.html', context)