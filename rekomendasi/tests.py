from django.test import TestCase
from django.urls import resolve
from . import views

class RekomendasiTest(TestCase):
    def test_url_exist(self):
        response = self.client.get('/rekomendasi/')
        print()
        self.assertEqual(response.status_code, 200)

    def test_correct_views_function(self):
        found = resolve('/rekomendasi/')
        self.assertEqual(found.func, views.rekomendasi)

    def test_using_right_template(self):
        response = self.client.get('/rekomendasi/')
        self.assertTemplateUsed(response, 'rekomendasi.html')

    




